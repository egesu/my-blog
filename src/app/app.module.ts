import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MarkdownModule } from 'ngx-markdown';
import { SpinnerComponentModule } from 'ng2-component-spinner';

import { SharedModule } from './shared';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogComponent } from './blog/blog.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';


@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    HomeComponent,
    ContactComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    MarkdownModule.forRoot(),
    SharedModule,
    SpinnerComponentModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
