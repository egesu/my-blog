import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable()
export class HttpService {
  constructor(
    private http: HttpClient
  ) { }

  public get(path: string, options = {}): Promise<any> {
    return this.http.get(environment.dataUrl.replace('{path}', path), options)
      .toPromise();
  }
}
