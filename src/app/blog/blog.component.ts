import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Blog } from './blog';
import { HttpService } from '../shared';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
  preserveWhitespaces: false,
})
export class BlogComponent implements OnInit, OnDestroy {
  private paramSubscriber: any;
  public slug: string;
  public blog: Blog;
  public spinnerRolls = false;

  constructor(
    private route: ActivatedRoute,
    private http: HttpService
  ) { }

  ngOnInit() {
    this.spinnerRolls = true;
    this.paramSubscriber = this.route.params.subscribe((params) => {
      this.slug = params.slug;
      this.http.get(params.slug + '.md', { responseType: 'text' })
        .then((response) => {
          this.blog = Blog.createFromMarkdown(response);
        })
        .catch((e) => console.log(e))
        .then(() => this.spinnerRolls = false);
    });
  }

  ngOnDestroy() {
    this.paramSubscriber.unsubscribe();
  }
}
