export class Blog {
  path: string;
  title: string;
  tags: string[];
  date: Date;
  text: string;

  public static createFromMarkdown(markdown: string): Blog {
    const blog = new Blog()

    if (markdown.indexOf('<!--') === 0) {
      // we have some meta data
      const closingCommentIndex = markdown.indexOf('-->');
      let commentContent = markdown.substring(0, closingCommentIndex - 1).split('\n');
      commentContent.shift();

      const text = markdown.substring(closingCommentIndex + 4).trim();
      blog.text = text;

      let lineArray: string[];
      let label: string;
      let content: string;

      for (const line of commentContent) {
        lineArray = line.split(':');
        label = lineArray.shift().trim();
        content = lineArray.join(':').trim();

        if (label === 'tags') {
          let tags = content.split(',');
          blog.tags = [];
          for (const tag of tags) {
            blog.tags.push(tag.trim());
          }
        } else if (label === 'date') {
          blog.date = new Date(content.trim());
        }
      }
    } else {
      // we don't have meta data, just set the text
      blog.text = markdown;
    }

    return blog;
  }
}
