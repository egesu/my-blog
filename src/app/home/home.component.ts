import { Component, OnInit } from '@angular/core';

import { HttpService } from '../shared';
import { Blog } from '../blog/blog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  preserveWhitespaces: false,
})
export class HomeComponent implements OnInit {
  public data: any[];
  public spinnerRolls = false;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.spinnerRolls = true;

    this.http.get('index.json', { responseType: 'json' })
      .then((response: Blog[]) => {
        this.data = response;
      })
      .catch()
      .then(() => this.spinnerRolls = false);
  }

  public getDate(dateString) {
    return new Date(dateString);
  }
}
