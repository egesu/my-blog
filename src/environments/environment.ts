// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  dataUrl: 'http://localhost:5000/{path}',
  // dataUrl: 'https://gitlab.com/api/v4/projects/egesu%2Fmy-blog-data/repository/files/{path}/raw?ref=master',
};
