export const environment = {
  production: true,
  dataUrl: 'https://gitlab.com/api/v4/projects/egesu%2Fmy-blog-data/repository/files/{path}/raw?ref=master',
};
