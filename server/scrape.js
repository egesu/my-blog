const { Chromeless } = require('chromeless');
const fs = require('fs');
const index = require('../dist/index.json');
const baseUrl = 'http://localhost:4200/';
const pages = [
  '',
  'contact',
];

for (var item in index) {
  pages.push(index[item].path);
}

async function run() {
  const chromeless = new Chromeless();
  let source;

  for (page of pages) {
    source = await chromeless
      .goto(baseUrl + page)
      .wait(2000)
      .html()
      .then((response) => {
        fs.writeFileSync('static/' + (page || 'index.html'), response);
      });
  }

  await chromeless.end();
}

run().catch(console.error.bind(console));